This repository is created to save and share the scripts that I used in my neuroscience projects for the creation of experimental stimuli and statistical analysis.

Those scripts are written in several languages due to different needs such as Matlab, Python, Java and Presentation.